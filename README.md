# OpenML dataset: CDP-Challenge-Cities-KPI

https://www.openml.org/d/43765

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset is generated after an epxloratory analysis of data provided by Carbon Disclosure Project in the following competition: CDP: Unlocking Climate Solutions.
It contains a compilation of indicators extracted and calculated based on cities' reponses to CDP disclosure for the year of 2020.
Content
Governance  Data Management

Sustainability.Targets.Master.Planning (Question 1.0): Cities report if they incorporate sustainability goals and targets into the master planning

Climate hazards and vulnerability

Risk.Assessment.Actions (Question 2.0): Cities report if a climate change risk or vulnerability assessment been undertaken
Nb.Hazards.Type (Question 2.1): Counting the number of reported hazards by organization
Hazards.Exposure.Level (Question 2.1): The weighted sum of reported hazards and porbability 
Adaptation.Challenges.Economic (Question 2.2): Number of reported significant adaptation challenges related to economic sector
Adaptation.Challenges.Health (Question 2.2): Number of reported significant adaptation challenges related to health sector
Adaptation.Challenges.Education (Question 2.2): Number of reported significant adaptation challenges related to education sector
Adaptation.Challenges.Habitat (Question 2.2): Number of reported significant adaptation challenges related to habitation sector
Adaptation.Challenges.Infrastructure (Question 2.2): Number of reported significant adaptation challenges related to infrastructure
Adaptation.Challenges.Social (Question 2.2): Number of reported significant  adaptation challenges related to social issues
Adaptation.Challenges.Environment (Question 2.2): Number of reported significant adaptation challenges related to environmental issues
Adaptation.Challenges.Governance (Question 2.2): Number of reported significant adaptation challenges related to governance
Adaptation.Challenges.Level (Question 2.2): Sum of reported significant adaptation challenges 
Risk.Health.System (Question 2.3): Cities indicate if they face risks to public  health systems associated with climate change

Adaptation

Nb.Adaptations.Actions (Question 3.0): Number of reported adaptation actions cities are taking to reduce the risk to climate change hazards
Adaptation.Plan (Question 3.2): Cities indicating if they have a published plan that addresses climate change adaptation

City-wide emissions

City.Wide.Emissions.Inventory (Question 4.0): Answering if they have a city-wide emissions inventory to report
GHG.Emissions.Primary.protocol (Question 4.3): The name of the primary protocol, standard, or methodology you have used to calculate your citys city-wide GHG emissions
GHG.Emissions.Evolution (Question 4.8): Indication if the city-wide emissions have increased, decreased, or stayed the same since the last emissions inventory
GHG.Emissions.Consumption (Question 4.9): Indication if the city-wide emissions have increased, decreased, or stayed the same since the last emissions inventory
GHG.Emissions.External.Verification (Question 4.12): Indication if the GHG emissions data heve been externally verified or audited

Emission reduction

GHG.Emissions.Reductions.Targets (Question 5.0):Cities are reporting if they have a GHG emissions reduction target(s) in place at the city-wide level
Emissions.Reductions.Mitigation.Planning (Question 5.5):Cities are reporting if they have a climate change mitigation or energy access plan for reducing city-wide GHG emissions

Opportunities

Opportunities.Collaboration (Question 6.2):Cities are reporting if they collaborate in partnership with businesses on sustainability projects

Energy

Renewable.Energy.Target (Question 8.0):Cities are reporting if they have a renewable energy or electricity target
Electricity.Source.Renewable (Question 8.1): Percent of renewable source mix of eleictricity consumed in the city
Energy.Efficnecy.Target (Question 8.5): Cities are reporting if they have a target to increase energy efficiency

Transport

Transport.Mode.Passenger.Private.motorized (Question 10.1): Percent of private motorized transport mode in city passenger transport
Transport.Mode.Passenger.Walking (Question 10.1): Percent of walking transport mode in city passenger transport
Transport.Mode.Passenger.Public (Question 10.1): Percent of public transport mode in city passenger transport
Transport.Mode.Passenger.Cycling (Question 10.1): Percent of cycling transport mode in city passenger transport
Transport.Mode.Passenger.Other (Question 10.1): Percent of other transport mode in city passenger transport
Low.Zero.Emission.Zone (Question 10.7): Cities are reporting if they have a low or zero-emission zone

Food

Food.Consumption.Policies (Question 12.3): Cities are reporting if they have any policies relating to food consumption

Water security

Potable.Water.Supply.Percent (Question 14.1): Percentage of city's population having potable water supply service
Potable.Water.Supply.Percent (Question 14.4): Cities are reporting if they have have a publicly available Water Resource Management strategy

Acknowledgements

This dataset is generated by this notebook: https://www.kaggle.com/shabou/exploratory-analysis-of-cities-responses
This datset is based on dataset provded by CDP challenge: https://www.kaggle.com/c/cdp-unlocking-climate-solutions

Inspiration

Multivariate analysis of cities' KPIs
Cities culstering based on the proposed KPIs

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43765) of an [OpenML dataset](https://www.openml.org/d/43765). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43765/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43765/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43765/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

